class SlideShow {
    /** Конструктор с аргументом data - это объект **/
    constructor(data = {}) {
        this.slide_show = document.getElementsByClassName(data['slideShowClass']); // data['slideShowClass'] - это класс слайда, в моём случае это slide
        this.start_count_slide = data['startCountSlide']; // data['startCountSlide'] - это порядковый номер слайда с которого начнётся слайдшоу 0...3                
	this.interval = data['intervalNextSlide']; // data['intervalNextSlide'] - это интервал смены слайда

        this.setData(); // Выполнение функции setData
        setInterval(this.nextSlide.bind(this), this.interval); // Таймер для выполнения функции nextSlide
    }
    
    /** Эта функция записывает data атрибуты в стили **/
    setData() {
        /** Перебираем все слайды **/
        Array.from(this.slide_show).forEach(result => {
            /** Если есть data атрибут color **/
	    if (result.dataset['color']) {
                result.style.backgroundColor = result.dataset.color; // Записываем свойство backgroundColor в стили текущего слайда
            } 

            /** Если есть data атрибут image **/
            if (result.dataset['image']) {
                var imageWidth = 'auto'; // Переменная содержит ширину слайда
                var imageHeight = 'auto'; // Переменная содержит высоту слайда

                /** Если data атрибут imageWidth присутствует **/
                if (result.dataset.imageWidth) {
                    imageWidth = result.dataset.imageWidth; // Записываем в переменную imageWidth ширину слайда
                }

                /** Если data атрибут imageHeight присутствует **/
                if (result.dataset.imageHeight) {
                    imageHeight = result.dataset.imageHeight; // Записываем в переменную imageHeight высоту слайда
                }

                /** Записываем свойства в стили с полученными значениями из data атрибутов **/
                result.style.backgroundImage = `url(${result.dataset.image}`;
                result.style.backgroundSize = `${imageWidth} ${imageHeight}`;
            }

            /** Если есть data атрибут htmlContent **/
            if (result.dataset['htmlContent']) {
                result.innerHTML = result.dataset.htmlContent; // Записываем в слайд HTML
            }

            /** Если есть data атрибут textContent **/
            if (result.dataset['textContent']) {
                result.textContent = result.dataset.textContent; // Записываем в слайд обычный текст
            }
        });
    }

    /** Эта функция переключает следующий слайд **/
    nextSlide() {
        this.slide_show[this.start_count_slide].className = 'slide'; // Меняем класс на текущем слайде
        this.start_count_slide = (this.start_count_slide + 1) % this.slide_show.length; // К переменной this.start_count_slide прибавляем 1
        this.slide_show[this.start_count_slide].className = 'slide show'; // У текущего слайда меняем класс
    }
}

slide_show = new SlideShow({
    'slideShowClass': 'slide', // Класс слайда
    'startCountSlide': 0, // С какого слайда начинать
    'intervalNextSlide': 1000, // Интервал смены слайда
});